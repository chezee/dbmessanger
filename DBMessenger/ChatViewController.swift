import UIKit
import JSQMessagesViewController

class ChatViewController: JSQMessagesViewController {
    var messages = [JSQMessage]()
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        let id: String = NSNumber(value: FetchController.shared.channelsArray.arrayOfChannels[0].users[0].id).stringValue
        self.senderId = id
        self.makeMessages()
        
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
    }
    
    //MARK: --
    //MARK: CollectionView
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }

    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    //MARK: --
    //MARK: Pressing send
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        if text.characters.count > 0 {
            let message: JSQMessage = JSQMessage.init(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
             messages.append(message)
            self.finishSendingMessage(animated: true)
        }
    }
    
    //MARK: --
    //MARK: Private methods
    
    private func makeMessages() {
        DispatchQueue.global(qos: .background).async {
            FetchController.shared.getMessegesData(completion: {
                for item in FetchController.shared.messegesArray {
                    let sender: User = User.init(with: item["sender"] as! [String : Any])
                    //print(sender.photoURL)
                    let id: String = NSNumber(value: sender.id).stringValue
                    
                    let dateString = item["create_date"] as! String
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "YYYY-MM-DD HH:mm:ss"
                    var array = dateString.components(separatedBy: "T")
                    var yearString = array[0]
                    let hourString = array[1]
                    array = hourString.components(separatedBy: ".")
                    yearString = yearString.appending(" \(array[0])")
                    let date = dateFormatter.date(from: yearString)
                    
                    let message: JSQMessage = JSQMessage.init(senderId: id,
                                                              senderDisplayName: sender.firstName,
                                                              date: date,
                                                              text: item["text"] as! String)
                    self.messages.append(message)
                }
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            })
        }
    }
}
