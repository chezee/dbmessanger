import UIKit

class FetchController: NSObject {
    
    static let shared = FetchController()
    let channels: String = "https://iostest.db2dev.com/api/chat/channels/"
    let messeges: String = "https://iostest.db2dev.com/api/chat/channels/1/messages/"
    var channelsArray: Channels = Channels()
    var messegesArray: Array<[String: Any]> = Array()
    
    //MARK: --
    //MARK: Lifecycle
    
    private override init() {}
    
    //MARK: --
    //MARK: Fetchmethods
    
    func getChannelsData(completion: @escaping () -> Void) {
        Fetch.fetchLast(from: channels, completion: {dict in
            let chan: Channels = Channels.init(with: dict["channels"] as! Array<Any>)
            self.channelsArray = chan
            completion()
        })
    }
    
    func getMessegesData(completion: @escaping () -> Void) {
        Fetch.fetchLast(from: messeges, completion: { dict in
            self.messegesArray = dict["messages"] as! Array<[String : Any]>
            completion()
        })
    }
}
