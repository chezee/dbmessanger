import UIKit

struct User {
    var firstName: String = ""
    var lastName: String = ""
    var id: Int = 0
    var photoURL: String = ""
    
    init(with dictionary:[String: Any]) {
        self.firstName = dictionary["first_name"] as! String
        self.id = dictionary["id"] as! Int
        self.lastName = dictionary["last_name"] as! String
        self.photoURL = dictionary["photo"] as! String
    }
    
    init() {
        
    }
}
