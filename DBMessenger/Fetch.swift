import UIKit

class Fetch: NSObject {
    
    //MARK: --
    //MARK: Abstract class for making universal request
    
    class func fetchLast(from link: String, completion: @escaping (Dictionary<String, Any>) -> Swift.Void) {
        let url: NSURL = NSURL(string: link)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        
        let username:String = "iostest"
        let password: String = "iostest2k17!"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Array<Any>] {
                    completion(json)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        })
        task.resume()
    }
}
