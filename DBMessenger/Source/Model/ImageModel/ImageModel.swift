import UIKit

class ImageModel: NSObject {
    var image: UIImage = UIImage()
    
    init(with link: URL) {
        super.init()
        
        DispatchQueue.global(qos: .background).async {
            do {
                let data: Data = try Data(contentsOf: link)
                self.image = UIImage.init(data: data)!

            } catch let error {
                print(error.localizedDescription)
            }
        }

    }
    

}
