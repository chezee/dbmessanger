import UIKit

class ChannelsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var unreadCounter: UILabel!
    
    var avatarImage: ImageModel? = nil
    
    let channel: Channel
    var avatarURL: URL? = nil
    
    //MARK: --
    //MARK: Lifecycle
    
    init(with channel: Channel) {
        super.init()
        self.channel = channel
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.makeCell()
        self.makeImage(from: avatarURL!)
    }
    
    //MARK: --
    //MARK: methods
    
    func makeCell() {
        let firstUser = self.channel.users[0]
        let firstName = firstUser.firstName
        let sirName = firstUser.lastName
        
        name.text = String(format: "%@ %@", firstName, sirName)
        message.text = self.channel.lastMessage.text
        
        self.avatarURL = URL(string: firstUser.photoURL)!
        self.makeImage(from: avatarURL!)
    
        if (self.channel.lastMessage.isRead == true) {
            unreadCounter.backgroundColor = UIColor.white
        } else {
            unreadCounter.text = NSNumber(value: self.channel.unreadMessages).stringValue
            unreadCounter.layer.cornerRadius = unreadCounter.frame.height / 2
            unreadCounter.layer.masksToBounds = true
        }
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        self.dateLabel.text = dateFormatter.string(from: channel.dateHour)
        self.makeDate()
    }
    
    func makeDate() {
        let date: Date = Date()
        let components: Set<Calendar.Component> = [.month, .day]
        let calendar = Calendar.current
        _ = calendar.dateComponents(components, from: self.channel.dateYear, to: date)
        
        //print("Difference: \(timeDifference)")
    }
    
    func makeImage(from avatarURL: URL) {
        self.avatarImage = ImageModel.init(with: avatarURL)
    }
}
