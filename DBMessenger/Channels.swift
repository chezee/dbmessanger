import UIKit

class Channels: NSObject {
    var arrayOfChannels: Array<Channel> = Array()
    
    var count: Int = 0
    
    init(with array: Array<Any>) {
        for item in array {
            self.count += 1
            let channel: Channel = Channel.init(with: item as! [String : Any])
            self.arrayOfChannels.append(channel)
        }
    }
    
    override init() {
        
    }
    
    //MARK: --
    //TODO: Make model class
    
    func addChanel(channel: Channel) {
        self.arrayOfChannels.append(channel)
    }
    
    func deleteChannel(channel: Channel) {
        let array: NSArray = self.arrayOfChannels as NSArray
        self.arrayOfChannels.remove(at: array.index(of: channel))
    }
    
    func deleteChannel(at index: Int) {
        self.arrayOfChannels.remove(at: index)
    }
    
    func channel(at index: Int) -> Channel {
        return self.arrayOfChannels[index]
    }
    
    func countOfChannels() -> Int {
        return self.arrayOfChannels.count
    }
}

struct Channel {
    
    let id: Int
    var users: Array<User> = Array()
    var lastMessage: LastMessage = LastMessage()
    let dateYear: Date
    let dateHour: Date
    var unreadMessages: Int = 0
    
    init(with dictionary: [String: Any]) {
        self.id = dictionary["id"] as! Int
        print("Last message: %@", dictionary["last_message"]!)
        self.lastMessage = LastMessage.init(dict: dictionary["last_message"] as! [String : Any])
        self.unreadMessages = dictionary["unread_messages_count"] as! Int
        let users: Array<[String: Any]> = dictionary["users"] as! Array<[String : Any]>
       
        let dateString = self.lastMessage.createDate
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        var array = dateString.components(separatedBy: "T")
        let yearString = array[0]
        self.dateYear = dateFormatter.date(from: yearString)!
        var hourString = array[1]
        array = hourString.components(separatedBy: ".")
        hourString = array[0]
        dateFormatter.dateFormat = "HH:mm:ss"
        self.dateHour = dateFormatter.date(from: hourString)!
        
        for item in users {
            let user: User = User.init(with: item)
            self.users.append(user)
        }
    }
    
    init() {
        self.id = 0
        self.dateHour = Date()
        self.dateYear = Date()
    }
    
}

struct LastMessage {
    let createDate: String
    let text: String
    var isRead: Bool = true
    var user: User = User()
    
    init(dict: [String:Any]) {
        print("Problem: %@", dict)
        self.text = dict["text"] as! String
        self.createDate = dict["create_date"] as! String
        self.user = User.init(with: dict["sender"] as! [String:Any])
        self.isRead = dict["is_read"] as! Bool
    }
    
    init() {
        createDate = String()
        text = String()
    }
}
