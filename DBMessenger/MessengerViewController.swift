import UIKit

class MessengerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var chatsTable: UITableView!
    @IBOutlet weak var tabBar: UITabBar!
    @IBOutlet weak var chatItem: UITabBarItem!
    
    let cellIdentifier = "messengerIdentifier"
    
    var channels: Channels = Channels()
    var readChannels: Channels = Channels()
    
    var unreadMessages: Int = 0 {
        willSet {
            self.chatItem.badgeValue = NSNumber(value: self.unreadMessages).stringValue
            if self.unreadMessages == 0 {
                self.chatItem.badgeValue = nil
            }
        }
    }
    
    //MARK: --
    //MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chatsTable.register(ChannelsTableViewCell.classForCoder(), forCellReuseIdentifier: cellIdentifier)
        chatsTable.register(UINib.init(nibName: "ChnnelsTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)

        self.fetchData()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.chatItem.badgeColor = UIColor.init(red: 0.313, green: 0.764, blue: 0.8901, alpha: 1)
        self.tabBar.selectedItem = self.tabBar.items?[0]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = false
        
        self.unreadMessages = 0
        for item in self.channels.arrayOfChannels {
            self.unreadMessages += item.unreadMessages
        }
        if self.unreadMessages == 0 {
            self.chatItem.badgeValue = nil
        }
        
        self.chatsTable.reloadData()
    }
    
    //MARK: --
    //MARK: TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return channels.countOfChannels()
        } else {
            return readChannels.countOfChannels()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 && channels.countOfChannels() != 0 {
            return 10
        } else {
            return 0
        }
    }
    
    //MARK: --
    //MARK: cell for row
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ChannelsTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! ChannelsTableViewCell
        if indexPath.section == 0 {
            cell = ChannelsTableViewCell.init(with: channels.arrayOfChannels[indexPath.row])
            self.unreadMessages = self.unreadMessages + cell.channel.unreadMessages
        } else {
            cell = ChannelsTableViewCell.init(with: readChannels.arrayOfChannels[indexPath.row])
            cell.makeCell()
        }
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chat = ChatViewController()
        var string = ""
        if indexPath.section == 0 {
            var channel: Channel = self.channels.channel(at: indexPath.row)
            
            string = channel.users[0].firstName
            channel.unreadMessages = 0
            channel.lastMessage.isRead = true
            
            self.readChannels.addChanel(channel: channel)
            self.channels.deleteChannel(at: indexPath.row)
        } else {
            string = self.readChannels.channel(at: indexPath.row).users[0].firstName
        }
        chat.senderDisplayName = string
        chat.title = string
        self.navigationController?.pushViewController(chat, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let swipe = UITableViewRowAction(style: .normal, title: "Remove", handler: {action, index in
        
        })
        swipe.backgroundColor = .blue
        
        return [swipe]
    }
    
    //MARK: --
    //MARK: private methods
    
    private func fetchData() {
        DispatchQueue.global(qos: .background).async {
            FetchController.shared.getChannelsData(completion: {
                for item in FetchController.shared.channelsArray.arrayOfChannels {
                    let channel: Channel = item
                    if channel.lastMessage.isRead {
                        self.readChannels.addChanel(channel: channel)
                    } else {
                        self.channels.addChanel(channel: channel)
                    }
                }
                DispatchQueue.main.async {
                    self.chatsTable.reloadData()
                }
            })
        }
    }
}
